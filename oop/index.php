<?php

require('Frog.php');
require('Ape.php');

$sheep = new animal("shaun");

echo "Name: " . $sheep->name . "<br>";
echo "Legs: " . $sheep->legs . "<br>";
echo "Cold Blooded: " . $sheep->cold_blooded . "<br>";

$frog = new Frog("buduk");

echo "<br>Name: " . $frog->name . "<br>";
echo "Legs: " . $frog->legs . "<br>";
echo "Cold Blooded: " . $frog->cold_blooded . "<br>";
echo "Jump: " . $frog->jump . "<br>";

$ape = new Ape("kera sakti");

echo "<br>Name: " . $ape->name . "<br>";
echo "Legs: " . $ape->legs . "<br>";
echo "Cold Blooded: " . $ape->cold_blooded . "<br>";
echo "Yell: " . $ape->yell . "<br>";