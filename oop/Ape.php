<?php

require_once('animal.php');
    class Ape extends animal{
        public $legs = 2;
        public $cold_blooded = "no";
        public $yell = "Auooo";
        public function __construct($name){
            $this->name = $name;
        }
        public function yell(){
            echo $this->yell;
        }
    }