<?php

require_once('animal.php');
    class Frog extends animal{
        public $legs = 4;
        public $cold_blooded = "no";
        public $jump = "hop hop";
        public function __construct($name){
            $this->name = $name;
        }
        public function jump(){
            echo $this->jump;
        }
    }