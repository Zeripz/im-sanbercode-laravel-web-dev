1 Membuat Database
create database myshop

2 Membuat Table di Dalam Database 
create table users(
    -> id integer auto_increment primary key,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
create table categories(
    -> id integer auto_increment primary key,
    -> name varchar(255));
create table items
    -> (id integer auto_increment primary key,
    -> name varchar(255),
    -> description varchar(255),
    -> price integer,
    -> stock integer,
    -> category_id integer,
    -> foreign key(category_id) references categories(id)
    -> );

3 Memasukkan Data pada Table 
insert into users(name,email,password) values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");
insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
insert into items(name,description,price,stock,category_id) values("Sumsang b50","hape keren dari merek sumsang","4000000","100","1"),("Uniklooh","baju keren dari brand ternama","500000","50","2"),("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");

4 Mengambil Data dari Database

-mengambil data users
SELECT id,name, email FROM users;

-mengambil data items
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name like "%uniklo%";

-Menampilkan data items join dengan kategori
SELECT items.name,items.description,items.price,items.stock,items.category_id, categories.name FROM items LEFT JOIN categories ON items.category_id =categories.id;

5 Mengubah Data dari Database
UPDATE items SET price=2500000 WHERE name = "Sumsang b50";