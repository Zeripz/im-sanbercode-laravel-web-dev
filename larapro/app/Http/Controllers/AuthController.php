<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }   
    public function kirim(Request $request){
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');

        return view('halaman.welcome', ['firstname' => $firstname, 'lastname' => $lastname]);
    }
}
