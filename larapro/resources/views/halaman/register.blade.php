@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name</label><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapore">Singapore</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br><br>
       <input type="submit" value="kirim">
    </form>
@endsection
    
